package skolkatalog;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;



	public class Main {
	
	static Scanner input = new Scanner(System.in);
	static int count = 0;

	
	public static void main(String[] args) {		
		
		enterData();
		showDb();
		
	}
	
	public static void enterData(){
		String firstname;
		String lastname;
		String email;
		String phone;
		
			System.out.println("Enter so many persons you like and then type stop to exit:" + "\n");
	
		for (int i = 0; i<500;i++){
			
			try {
				// 1. Get a connection to database
				Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/skolkatalog", "root" , "mikaelf");
				// 2. Create a statement
				Statement stmt = myConn.createStatement();
			
			System.out.println("Enter firstname:");
			firstname = input.next();					
			if (firstname.equalsIgnoreCase("stop"))break;
		
			System.out.println("Enter lastname:");
			lastname = input.next();
			if (lastname.equalsIgnoreCase("stop"))break;
			
			System.out.println("Enter email:");
			email = input.next();
			if (email.equalsIgnoreCase("stop"))break;
			
			System.out.println("Enter phone number:");
			phone = input.next();
			if (phone.equalsIgnoreCase("stop"))break;

			Elev person = new Elev (firstname,lastname,email,phone);
			
			String tempFname = person.getFirstname();
			String tempLname = person.getLastname();
			String tempEmail = person.getEmail();
			String tempPhone = person.getPhone();
			String sql = "INSERT INTO studentlista (firstname, lastname, email, phone) ";
			sql += "VALUES('"+tempFname+"','"+tempLname+"','"+tempEmail+"','"+tempPhone+"')";
			stmt.executeUpdate(sql);
		count++;
		
		}
			catch (Exception exc) {
				exc.printStackTrace();
				}
		}
		
	}
	public static void showDb(){
		try {
			// 1. Get a connection to database
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/skolkatalog", "root" , "mikaelf");
			// 2. Create a statement
			Statement stmt = myConn.createStatement();
		
			String sql = "SELECT * FROM studentlista";
			ResultSet resultSet = stmt.executeQuery(sql);
			while(resultSet.next()){
			System.out.println("Data from database: " 
								+ resultSet.getString("ID")+ " " 
								+ resultSet.getString("Firstname") + " " 
								+ resultSet.getString("Lastname")+" " 
								+ resultSet.getString("Email")+ " " 
								+ resultSet.getString("Phone"));
			}
		}
		
		catch (Exception exc) {
			exc.printStackTrace();
			}
			
	}
}