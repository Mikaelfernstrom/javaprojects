package skolkatalog;

public class Elev {
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	
	public Elev(String firstname, String lastname, String email, String phone){
		
		this.firstname = firstname;  // constructor
		this.lastname = lastname;
		this.email = email;
		this.phone = phone;
	}
	public Elev(){  // constructor
		
		this.firstname = "";
		this.lastname = "";
		this.email = "";
		this.phone = "";
	}
	
	public String getElevinfo(){
		return firstname+ " " + lastname+ " " + email+ " " + phone;
	}
	
	public String getFirstname(){
		return firstname;	
	}
	public String getLastname(){
		return lastname;	
	}
	public String getEmail(){
		return email;	
	}
	public String getPhone(){
		return phone;	
	}
	public void setFirstname(String f){
		this.firstname = f;
	}
	public void setLastname(String l){
		this.lastname = l;
	}
	public void setEmail(String e){
		this.email = e;
	}
	public void setPhone(String p){
		this.phone = p;
	}

}